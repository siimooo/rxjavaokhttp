package io.rxjavaokhttp;

import okhttp3.HttpUrl;
import okhttp3.Response;
import org.junit.Before;
import org.junit.Test;
import rx.Subscriber;
import rx.observers.TestSubscriber;

import java.io.IOException;

/**
 * Test methods
 *
 * User: Simo Ala-Kotila
 * Date: 14/11/16
 * Time: 19:42
 */
public class BaseClientTest {

    private String host = "google.com";
    private int port443 = 443;
    private int port80 = 80;

    @Test
    public void testSuccessHTTP(){
        final TestSubscriber<Response> ts = new TestSubscriber<>();
        DummyClass dummyClass = new DummyClass(host, port80, BaseClient.HTTP_SCHEMA);
        dummyClass.get().subscribe(new Subscriber<Response>() {
            @Override
            public void onCompleted() {
                ts.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                ts.onError(e);
            }

            @Override
            public void onNext(Response response) {
                ts.onNext(response);
            }
        });

        ts.awaitTerminalEvent();
        ts.assertNoErrors();
    }

    @Test
    public void testSuccessHTTPS3(){
        final TestSubscriber<Response> ts = new TestSubscriber<>();
        DummyClass dummyClass = new DummyClass(host);
        dummyClass.get().subscribe(new Subscriber<Response>() {
            @Override
            public void onCompleted() {
                ts.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                ts.onError(e);
            }

            @Override
            public void onNext(Response response) {
                ts.onNext(response);
            }
        });

        ts.awaitTerminalEvent();
        ts.assertNoErrors();
    }



    @Test
    public void testSuccessHTTPS(){
        final TestSubscriber<Response> ts = new TestSubscriber<>();
        DummyClass dummyClass = new DummyClass(host, port443, BaseClient.HTTPS_SCHEMA);
        dummyClass.get().subscribe(new Subscriber<Response>() {
            @Override
            public void onCompleted() {
                ts.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                ts.onError(e);
            }

            @Override
            public void onNext(Response response) {
                ts.onNext(response);
            }
        });

        ts.awaitTerminalEvent();
        ts.assertNoErrors();
    }

}
