package io.rxjavaokhttp;

/**
 * TODO: write here description
 * User: Simo Ala-Kotila
 * Date: 14/11/16
 * Time: 19:43
 */
public class DummyClass extends BaseClient {


    public DummyClass(String host, int port, String schema) {
        super(host, port, schema);
    }

    public DummyClass(String host) {
        super(host);
    }
}
