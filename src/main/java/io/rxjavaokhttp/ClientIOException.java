package io.rxjavaokhttp;

import java.io.IOException;

/**
 * Exception object for the IOExceptions
 *
 * User: Simo Ala-Kotila
 * Date: 14/11/16
 * Time: 19:35
 */
public class ClientIOException extends IOException {

    public static final int UNKNOWN = 101;

    private final int errorCode;

    private final String url;

    /**
     * Constructs an {@code IOException} with the specified detail message.
     *
     * @param message
     *        The detail message (which is saved for later retrieval
     *        by the {@link #getMessage()} method)
     * @param url
     */
    public ClientIOException(String message, int errorCode, String url) {
        super(message);
        this.errorCode = errorCode;
        this.url = url;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getUrl() {
        return url;
    }
}
