package io.rxjavaokhttp;

import okhttp3.*;
import rx.Observable;
import rx.Subscriber;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Abstract base client for API's
 *
 * User: Simo Ala-Kotila
 * Date: 10/04/16
 * Time: 10:40
 */
public abstract class BaseClient {

    private final ConnectionPool pool = new ConnectionPool();
    public static final int DOWNLOAD_CHUNK_SIZE = 2048; //Same as Okio Segment.SIZE

    public static final String HTTP_SCHEMA = "http";
    public static final String HTTPS_SCHEMA = "https";
    public static final String FTP_SCHEMA = "ftp";

    protected final OkHttpClient client;
    protected final String host;
    protected final int port;
    protected final String schema;

    protected static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    protected static final MediaType OCTET = MediaType.parse("application/octet-stream; charset=utf-8");
    protected static final MediaType TAR = MediaType.parse("application/tar; charset=utf-8");

    public static final int DEFAULT_IMEOUT_VALUE = 10;

    /**
     *
     * @param host
     * @param port
     * @param schema
     */
    public BaseClient(String host, int port, String schema){
        this.host = host;
        this.port = port;
        this.schema = schema;
        this.client = okHttpClientBuilder().build();
    }

    public BaseClient(String host){
        this(host, 443, "https");
    }

    protected OkHttpClient.Builder okHttpClientBuilder(){
        return new OkHttpClient.Builder()
                .connectTimeout(DEFAULT_IMEOUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_IMEOUT_VALUE, TimeUnit.SECONDS)
                .connectTimeout(DEFAULT_IMEOUT_VALUE, TimeUnit.SECONDS)
                .connectionPool(pool);
    }

    /**
     * Base for GET request
     *
     * @param builder
     * @return
     */
    protected Request.Builder makeGetRequestBuilder(HttpUrl.Builder builder) throws IOException {
        return new Request.Builder().url(builder.host(host).port(port).scheme(schema).build().url());
    }

    private Response makeGetRequest(HttpUrl.Builder builder) throws IOException {
        return client.newCall(makeGetRequestBuilder(builder).build()).execute();
    }

    public void post(){ }

    public void delete(){ }

    public Observable<Response> get(){
        return get(new HttpUrl.Builder());
    }

    public Observable<Response> get(final HttpUrl.Builder builder){
        return Observable.create(new Observable.OnSubscribe<Response>() {
            @Override
            public void call(Subscriber<? super Response> subscriber) {
                try {
                    Response response = makeGetRequest(builder);
                    String url = response.request().url().toString();
                    if(response.isSuccessful()){
                        subscriber.onNext(response);
                        subscriber.onCompleted();
                    }else if (response.isSuccessful() && !subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                    }else if(!response.isSuccessful()){
                        int statusCode = response.code();
                        if(response.body() == null){
                            throw new ClientIOException(response.message(), statusCode, url);
                        }
                        throw new ClientIOException(response.body().string(), statusCode, url);
                    }else {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onError(new ClientIOException(String.format("get returned %d with message %s", response.code(), response.message()), ClientIOException.UNKNOWN, url));
                        }
                    }
                }catch (IOException e){
                    subscriber.onError(e);
                }
            }
        });
    }

    public void put(){ }
}